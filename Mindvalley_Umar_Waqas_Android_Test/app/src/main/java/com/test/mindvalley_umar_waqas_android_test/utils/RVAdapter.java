package com.test.mindvalley_umar_waqas_android_test.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.test.mindvalley_umar_waqas_android_test.R;
import com.test.mindvalley_umar_waqas_android_test.app.AppController;
import com.test.mindvalley_umar_waqas_android_test.model.ImageDataModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Umar Waqas on 12/7/2016.
 */

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.CustomViewHolder> {

    List<ImageDataModel> imagesList;
    Activity mContext;
    private LruCache<String, Bitmap> imageCache;
    public RVAdapter(Activity context, List<ImageDataModel> list) {
        this.imagesList = list;
        this.mContext = context;
        final int maxMemory = (int)(Runtime.getRuntime().maxMemory() /1024);
        final int cacheSize = maxMemory / 8;
        imageCache = new LruCache<>(cacheSize);
    }
    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_item, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, int position) {
        try {
            final int pos=position;

            final ImageDataModel currentImaeModel = imagesList.get(position);
            Bitmap bitmap = imageCache.get(currentImaeModel.getImageId());

            if (bitmap != null) {
                holder.Image.setImageBitmap(bitmap);
            }
            else {
                String imageUrl = currentImaeModel.getImageUrls().getUrlThumbnail();
                ImageRequest request = new ImageRequest(imageUrl,
                        new Response.Listener<Bitmap>() {

                            @Override
                            public void onResponse(Bitmap arg0) {
                                holder.Image.setImageBitmap(arg0);
                                imageCache.put(currentImaeModel.getImageId(), arg0);
                            }
                        },
                        200, holder.Image.getLayoutParams().height= Utilities.getImageHeight(mContext),
                        Bitmap.Config.ARGB_8888,

                        new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError arg0) {
                                Log.d("FlowerAdapter", arg0.getMessage());
                            }
                        }
                );
                AppController.getInstance().addToRequestQueue(request,
                        currentImaeModel.getImageId());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return imagesList.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder{
        private TextView title;
      //  private TextView desc;
        private TextView dur;
        private ImageView Image,fvImage;
        RelativeLayout rlImage;
        public CustomViewHolder(View v) {
            super(v);
            Image = (ImageView) v.findViewById(R.id.rec_vid_img);}
    }
}



