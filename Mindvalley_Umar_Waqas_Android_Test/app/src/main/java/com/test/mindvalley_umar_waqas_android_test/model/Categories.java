package com.test.mindvalley_umar_waqas_android_test.model;

/**
 * Created by Umar on 23-Jan-17.
 */
public class Categories {
    public static final String KEY_CATEGORIES = "categories";
    public static final String KEY_CATEGORY_ID = "id";
    public static final String KEY_CATEGORY_TITLE = "title";
    public static final String KEY_CATEGORY_PHOTO_COUNT = "photo_count";


    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryTitle() {
        return categoryTitle;
    }

    public void setCategoryTitle(String categoryTitle) {
        this.categoryTitle = categoryTitle;
    }

    public String getCategoryPhotoCount() {
        return categoryPhotoCount;
    }

    public void setCategoryPhotoCount(String categoryPhotoCount) {
        this.categoryPhotoCount = categoryPhotoCount;
    }

    private int categoryId;
    private String categoryTitle;
    private String categoryPhotoCount;
    private LinksModel categoryImageLinks;

    public void setCategoryImageLinks(LinksModel model) {
        this.categoryImageLinks = model;
    }

    public LinksModel getCategoryImageLinks() {
        return categoryImageLinks;
    }
}
