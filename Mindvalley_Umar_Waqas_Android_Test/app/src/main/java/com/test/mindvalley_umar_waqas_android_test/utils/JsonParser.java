package com.test.mindvalley_umar_waqas_android_test.utils;

import com.test.mindvalley_umar_waqas_android_test.model.Categories;
import com.test.mindvalley_umar_waqas_android_test.model.ImageDataModel;
import com.test.mindvalley_umar_waqas_android_test.model.ImageUrls;
import com.test.mindvalley_umar_waqas_android_test.model.LinksModel;
import com.test.mindvalley_umar_waqas_android_test.model.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.test.mindvalley_umar_waqas_android_test.model.Categories.*;
import static com.test.mindvalley_umar_waqas_android_test.model.ImageDataModel.*;
import static com.test.mindvalley_umar_waqas_android_test.model.ImageUrls.*;
import static com.test.mindvalley_umar_waqas_android_test.model.LinksModel.*;
import static com.test.mindvalley_umar_waqas_android_test.model.UserModel.*;

/**
 * Created by Umar on 24-Jan-17.
 */

public class JsonParser {


    public List<ImageDataModel> parseJson(JSONArray jsnAry) {
        List<ImageDataModel> imagesList=new ArrayList<>();
        if (jsnAry != null) {
            try {
                for (int i = 0; i < jsnAry.length(); i++) {
                    JSONObject jsonObject = jsnAry.getJSONObject(i);
                    ImageDataModel imageModel = new ImageDataModel();
                    imageModel.setImageId(jsonObject.getString(KEY_IMAGE_ID));
                    imageModel.setImageCreatedTime(jsonObject.getString(KEY_IMAGE_CREATE));
                    imageModel.setImageWidth(jsonObject.getInt(KEY_IMAGE_WIDTH));
                    imageModel.setImageHeight(jsonObject.getInt(KEY_IMAGE_HEIGHT));
                    imageModel.setImageColor(jsonObject.getString(KEY_IMAGE_COLOR));
                    imageModel.setImageLikes(jsonObject.getInt(KEY_IMAGE_LIKE));
                    imageModel.setLikedBy(jsonObject.getBoolean(KEY_IMAGE_LIKE_BY));

                    imageModel.setUserModel(parseUserData(jsonObject.getJSONObject(KEY_USER)));

                    imageModel.setImageUrls(parseImageUrls(jsonObject.getJSONObject(KEY_URLS)));

                    imageModel.setCategoriesList(
                            parseCategoriesData(jsonObject.getJSONArray(KEY_CATEGORIES)));

                    imageModel.setImageApiLinks(parseImageLinks(jsonObject));

                    imagesList.add(imageModel);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {

            }

        }

        return imagesList;
    }


    private UserModel parseUserData(JSONObject jsonObject) throws JSONException {
        UserModel model=null;
        if (jsonObject != null) {
            model=new UserModel();
            model.setUserId(jsonObject.getString(KEY_USER_ID));
            model.setUserName(jsonObject.getString(KEY_USER_USER_NAME));
            model.setNameOfUser(jsonObject.getString(KEY_USER_NAME));
            JSONObject profileJsn=jsonObject.getJSONObject(KEY_PROFILE_IMAGE);
            model.setProfileImageSmall(profileJsn.getString(KEY_PROFILE_SMALL_IMAGE));
            model.setProfileImageMedium(profileJsn.getString(KEY_PROFILE_MEDIUD_IMAGE));
            model.setProfileImageLarge(profileJsn.getString(KEY_PROFILE_LARGE_IMAGE));

            LinksModel linksModel=new LinksModel();
            JSONObject linksJsn=jsonObject.getJSONObject(KEY_LINK);
            linksModel.setLinkSelf(linksJsn.getString(KEY_LINK_SELF));
            linksModel.setLinkHtml(linksJsn.getString(KEY_LINK_HTML));
            linksModel.setLinkPhotos(linksJsn.getString(KEY_LINK_PHOTOS));
            linksModel.setLinkLikes(linksJsn.getString(KEY_LINK_LIKES));
            model.setLinksModel(linksModel);
        }


        return model;
    }
    private ImageUrls parseImageUrls(JSONObject jsonObject) throws JSONException{
        ImageUrls model=new ImageUrls();
        if (jsonObject != null) {
            model.setUrlFull(jsonObject.getString(KEY_URL_FULL));
            model.setUrlRaw(jsonObject.getString(KEY_URL_RAW));
            model.setUrlRegular(jsonObject.getString(KEY_URL_REGULAR));
            model.setUrlSmall(jsonObject.getString(KEY_URL_SMALL));
            model.setUrlThumbnail(jsonObject.getString(KEY_URL_THUMBNAIL));
        }

        return model;
    }

    private List<Categories> parseCategoriesData(JSONArray jsonArray) throws JSONException{
        Categories model=null;
        List<Categories> categoriesList=new ArrayList<>();
        if (jsonArray != null) {
            for (int i=0; i<jsonArray.length();i++) {
                JSONObject json=jsonArray.getJSONObject(i);
                model=new Categories();
                model.setCategoryId(json.getInt(KEY_CATEGORY_ID));
                model.setCategoryTitle(json.getString(KEY_CATEGORY_TITLE));
                model.setCategoryPhotoCount(json.getString(KEY_CATEGORY_PHOTO_COUNT));

                LinksModel linksModel=new LinksModel();
                JSONObject linksJsn=json.getJSONObject(KEY_LINK);
                linksModel.setLinkSelf(linksJsn.getString(KEY_LINK_SELF));

                linksModel.setLinkPhotos(linksJsn.getString(KEY_LINK_PHOTOS));
                model.setCategoryImageLinks(linksModel);
                categoriesList.add(model);

            }
        }


        return categoriesList;
    }
    private LinksModel parseImageLinks(JSONObject jsonObject) throws JSONException{
        LinksModel linksModel=null;
        if (jsonObject != null) {
            linksModel=new LinksModel();
            JSONObject linksJsn=jsonObject.getJSONObject(KEY_LINK);
            linksModel.setLinkSelf(linksJsn.getString(KEY_LINK_SELF));
            linksModel.setLinkHtml(linksJsn.getString(KEY_LINK_HTML));
            linksModel.setLinkDownload(linksJsn.getString(KEY_LINK_DOWNLOAD));

        }


        return linksModel;
    }


}//end of class....
