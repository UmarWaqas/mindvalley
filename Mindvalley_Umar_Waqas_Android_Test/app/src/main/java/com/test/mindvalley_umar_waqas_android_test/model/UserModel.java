package com.test.mindvalley_umar_waqas_android_test.model;

/**
 * Created by Umar on 23-Jan-17.
 */
public class UserModel {
    //Keys for getting user data....
    public static final String KEY_USER = "user";

    public static final String KEY_USER_ID = "id";
    public static final String KEY_USER_USER_NAME = "username";
    public static final String KEY_USER_NAME = "name";

    public static final String KEY_PROFILE_IMAGE = "profile_image";
    public static final String KEY_PROFILE_SMALL_IMAGE = "small";
    public static final String KEY_PROFILE_MEDIUD_IMAGE = "medium";
    public static final String KEY_PROFILE_LARGE_IMAGE = "large";

    // variables for user attributes....
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getNameOfUser() {
        return nameOfUser;
    }

    public void setNameOfUser(String nameOfUser) {
        this.nameOfUser = nameOfUser;
    }

    public String getProfileImageSmall() {
        return profileImageSmall;
    }

    public void setProfileImageSmall(String profileImageSmall) {
        this.profileImageSmall = profileImageSmall;
    }

    public String getProfileImageMedium() {
        return profileImageMedium;
    }

    public void setProfileImageMedium(String profileImageMedium) {
        this.profileImageMedium = profileImageMedium;
    }

    public String getProfileImageLarge() {
        return profileImageLarge;
    }

    public void setProfileImageLarge(String profileImageLarge) {
        this.profileImageLarge = profileImageLarge;
    }

    public LinksModel getLinksModel() {
        return linksModel;
    }

    public void setLinksModel(LinksModel linksModel) {
        this.linksModel = linksModel;
    }

    private String userId;
    private String userName;
    private String nameOfUser;
    private String profileImageSmall;
    private String profileImageMedium;
    private String profileImageLarge;
    private LinksModel linksModel;


}//end of class UserModel...
