package com.test.mindvalley_umar_waqas_android_test.model;

/**
 * Created by Umar on 23-Jan-17.
 */
public class LinksModel {

    // Keys for getting links....
    public static final String KEY_LINK = "links";
    public static final String KEY_LINK_SELF = "self";
    public static final String KEY_LINK_HTML = "html";
    public static final String KEY_LINK_PHOTOS = "photos";
    public static final String KEY_LINK_LIKES = "likes";
    public static final String KEY_LINK_DOWNLOAD = "download";


    public String getLinkSelf() {
        return linkSelf;
    }

    public void setLinkSelf(String linkSelf) {
        this.linkSelf = linkSelf;
    }

    public String getLinkHtml() {
        return linkHtml;
    }

    public void setLinkHtml(String linkHtml) {
        this.linkHtml = linkHtml;
    }

    public String getLinkPhotos() {
        return linkPhotos;
    }

    public void setLinkPhotos(String linkPhotos) {
        this.linkPhotos = linkPhotos;
    }

    public String getLinkLikes() {
        return linkLikes;
    }

    public void setLinkLikes(String linkLikes) {
        this.linkLikes = linkLikes;
    }

    public String getLinkDownload() {
        return linkDownload;
    }

    public void setLinkDownload(String linkDownload) {
        this.linkDownload = linkDownload;
    }


    // variables for links....
    private String linkSelf;
    private String linkHtml;
    private String linkPhotos;
    private String linkLikes;
    private String linkDownload;

}//end of class LinksModel....
