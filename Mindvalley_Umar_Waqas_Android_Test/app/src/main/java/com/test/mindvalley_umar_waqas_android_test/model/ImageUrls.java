package com.test.mindvalley_umar_waqas_android_test.model;

/**
 * Created by Umar on 23-Jan-17.
 */
public class ImageUrls {
    public static final String KEY_URLS = "urls";
    public static final String KEY_URL_RAW = "raw";
    public static final String KEY_URL_FULL = "full";
    public static final String KEY_URL_REGULAR = "regular";
    public static final String KEY_URL_SMALL = "small";
    public static final String KEY_URL_THUMBNAIL = "thumb";

    public String getUrlRaw() {
        return urlRaw;
    }

    public void setUrlRaw(String urlRaw) {
        this.urlRaw = urlRaw;
    }

    public String getUrlFull() {
        return urlFull;
    }

    public void setUrlFull(String urlFull) {
        this.urlFull = urlFull;
    }

    public String getUrlRegular() {
        return urlRegular;
    }

    public void setUrlRegular(String urlRegular) {
        this.urlRegular = urlRegular;
    }

    public String getUrlSmall() {
        return urlSmall;
    }

    public void setUrlSmall(String urlSmall) {
        this.urlSmall = urlSmall;
    }

    public String getUrlThumbnail() {
        return urlThumbnail;
    }

    public void setUrlThumbnail(String urlThumbnail) {
        this.urlThumbnail = urlThumbnail;
    }

    private String urlRaw;
    private String urlFull;
    private String urlRegular;
    private String urlSmall;
    private String urlThumbnail;

}
