package com.test.mindvalley_umar_waqas_android_test.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.test.mindvalley_umar_waqas_android_test.R;
import com.test.mindvalley_umar_waqas_android_test.app.AppController;
import com.test.mindvalley_umar_waqas_android_test.model.ImageDataModel;
import com.test.mindvalley_umar_waqas_android_test.utils.JsonParser;
import com.test.mindvalley_umar_waqas_android_test.utils.RVAdapter;
import com.test.mindvalley_umar_waqas_android_test.utils.Utilities;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {

	RecyclerView mRecyclerView;
	SwipeRefreshLayout refreshLayout;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mRecyclerView= (RecyclerView) findViewById(R.id.recycler_view);
		refreshLayout= (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
		refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh()
			{
				new GetImages().getImageJson();
			}
		});

		new GetImages().getImageJson();


	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}


	public class GetImages  {

		List<ImageDataModel> imageDataModelList;
		public  String JSON_TAG="image_json";
		public List<ImageDataModel>  getImageJson() {
					refreshLayout.setRefreshing(true);
			JsonArrayRequest request = new JsonArrayRequest(Utilities.TEST_URL,
					new Response.Listener<JSONArray>() {
						@Override
						public void onResponse(JSONArray response) {
							imageDataModelList=new ArrayList<>();
							if (response!=null)
								imageDataModelList= new JsonParser().parseJson(response);
							mRecyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
							mRecyclerView.setAdapter(new RVAdapter(MainActivity.this,imageDataModelList));
							refreshLayout.setRefreshing(false);
						}
					}, new Response.ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {

				}
			});

			// Adding request to request queue
			AppController.getInstance().addToRequestQueue(request,
					JSON_TAG);

			// Cancelling request
			// ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_arry);
			return imageDataModelList;
		}

	}
}
