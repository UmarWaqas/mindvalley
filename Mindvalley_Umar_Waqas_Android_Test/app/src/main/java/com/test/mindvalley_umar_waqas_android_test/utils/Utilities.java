package com.test.mindvalley_umar_waqas_android_test.utils;

import android.app.Activity;
import android.util.DisplayMetrics;

public class Utilities {
	public static final String URL_JSON_OBJECT = "http://api.androidhive.info/volley/person_object.json";
	public static final String URL_JSON_ARRAY = "http://api.androidhive.info/volley/person_array.json";
	public static final String URL_STRING_REQ = "http://api.androidhive.info/volley/string_response.html";
	public static final String URL_IMAGE = "http://api.androidhive.info/volley/volley-image.jpg";
	public static final String TEST_URL = "http://pastebin.com/raw/wgkJgazE";


	public static int getImageHeight(Activity mActivity) {
		DisplayMetrics displaymetrics = new DisplayMetrics();

		mActivity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

		// int screenWidth = displaymetrics.widthPixels;

		int screenHeight = displaymetrics.heightPixels;
		int height = new Double(screenHeight * 0.40).intValue();

		return height;

	}
}
