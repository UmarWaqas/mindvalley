package com.test.mindvalley_umar_waqas_android_test.model;

import java.util.List;

/**
 * Created by Umar on 23-Jan-17.
 */

public class ImageDataModel {

    //Keys for getting image data....
    public static final String KEY_IMAGE_ID = "id";
    public static final String KEY_IMAGE_CREATE = "created_at";
    public static final String KEY_IMAGE_WIDTH = "width";
    public static final String KEY_IMAGE_HEIGHT = "height";
    public static final String KEY_IMAGE_COLOR = "color";
    public static final String KEY_IMAGE_LIKE = "likes";
    public static final String KEY_IMAGE_LIKE_BY = "liked_by_user";

    // variables for image attributes....
    public  String        getImageId() {
        return imageId;
    }public void       setImageId(String imageId) {
        this.imageId = imageId;
    }public int        getImageWidth() {
        return imageWidth;
    }public void       setImageWidth(int imageWidth) {
        this.imageWidth = imageWidth;
    }public int        getImageHeight() {
        return imageHeight;
    }public void       setImageHeight(int imageHeight) {
        this.imageHeight = imageHeight;
    }public int        getImageLikes() {
        return imageLikes;
    }public void       setImageLikes(int imageLikes) {
        this.imageLikes = imageLikes;
    }public String     getImageCreatedTime() {
        return imageCreatedTime;
    }public void       setImageCreatedTime(String imageCreatedTime) {
        this.imageCreatedTime = imageCreatedTime;
    }public String     getImageColor() {
        return imageColor;
    }public void       setImageColor(String imageColor) {
        this.imageColor = imageColor;
    }public boolean    isLikedBy() {
        return likedBy;
    }public void       setLikedBy(boolean likedBy) {
        this.likedBy = likedBy;
    }public UserModel  getUserModel() {
        return userModel;
    }public void       setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }public ImageUrls  getImageUrls() {
        return imageUrls;
    }public void       setImageUrls(ImageUrls imageUrls) {
        this.imageUrls = imageUrls;
    }public List<Categories> getCategoriesList() {
        return categoriesList;
    }public void setCategoriesList(List<Categories> categoriesList) {
        this.categoriesList = categoriesList;
    }public LinksModel getImageApiLinks() {
        return imageApiLinks;
    }public void       setImageApiLinks(LinksModel imageApiLinks) {
        this.imageApiLinks = imageApiLinks;
    }

    private String imageId;
    private int imageWidth;
    private int imageHeight;
    private int imageLikes;
    private String imageCreatedTime;
    private String imageColor;
    private boolean likedBy;
    private UserModel userModel;
    private ImageUrls imageUrls;
    private List<Categories> categoriesList;
    private LinksModel imageApiLinks;


}//end of class ImageDataModel....